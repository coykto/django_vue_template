// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable no-unused-vars, no-new */
import Vue from "vue"
import App from "./App"
import axios from "axios"

Vue.config.productionTip = false

var articlesApp = new Vue({
  el: "#app",
  components: { App },
  template: "<App/>",
  data () {
    return {
      articles: []
    }
  },
  mounted: function () {
    this.getArticles()
  },
  methods: {
    getArticles: function () {
      axios.get("/api/articles")
        .then((response) => {
          this.header = response.data
        }).catch((err) => {
          console.log(err)
        })
    }
  }
})
