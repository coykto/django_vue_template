from rest_framework import routers
from apps.articles.viewsets import ArticleViewSet


router = routers.DefaultRouter()

router.register(r'articles', ArticleViewSet)
