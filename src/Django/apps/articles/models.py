from django.db import models


# Create your models here.
class Article(models.Model):
    header = models.CharField(max_length=150)
    body = models.TextField()
