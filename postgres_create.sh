#!/usr/bin/env bash
docker run -p 5432:5432 --name azure_db -d postgres
sleep 5
docker exec azure_db psql -U postgres -c "CREATE role coykto with login password '1AbE307388521991' superuser;"
docker exec azure_db psql -U postgres -c "CREATE DATABASE azure_db;"
